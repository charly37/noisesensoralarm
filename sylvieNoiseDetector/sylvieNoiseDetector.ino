//Idea : Sylvie

//pin guirlande led
const int _OutPinBuzer = 4;
const int _InPinSoundsDetector = A0;
const int _InPinAlarmLevel = A0;

int valS1 = 0;
int valS2 = 0;
int threshold = 0;

//smoothing
// Define the number of samples to keep track of.  The higher the number,
// the more the readings will be smoothed, but the slower the output will
// respond to the input.  Using a constant rather than a normal variable lets
// use this value to determine the size of the readings array.
const int numReadings = 10;

//sensor1
int readings1[numReadings];      // the readings from the analog input
int readIndex1 = 0;              // the index of the current reading
int total1 = 0;                  // the running total
int average1 = 0;                // the average

//sensor2
int readings2[numReadings];      // the readings from the analog input
int readIndex2 = 0;              // the index of the current reading
int total2 = 0;                  // the running total
int average2 = 0;                // the average

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(_OutPinBuzer, OUTPUT);
  pinMode (A0, INPUT) ;
  pinMode (A2, INPUT) ;

  digitalWrite(_OutPinBuzer, LOW);

  // initialize all the readings to 0:
  for (int thisReading = 0; thisReading < numReadings; thisReading++) {
    readings1[thisReading] = 0;
    readings2[thisReading] = 0;
  }
}

void loop() {
  // put your main code here, to run repeatedly:
  valS1 = analogRead(A0);
  valS2 = analogRead(A2);
  
  Serial.print(valS1);
  Serial.print(";");
  Serial.print(valS2);

  //https://www.arduino.cc/en/Tutorial/Smoothing

//sensor1
  // subtract the last reading:
  total1 = total1 - readings1[readIndex1];
  // read from the sensor:
  readings1[readIndex1] = valS1;
  // add the reading to the total:
  total1 = total1 + readings1[readIndex1];
  // advance to the next position in the array:
  readIndex1 = readIndex1 + 1;

  // if we're at the end of the array...
  if (readIndex1 >= numReadings) {
    // ...wrap around to the beginning:
    readIndex1 = 0;
  }
    // calculate the average:
  average1 = total1 / numReadings;
  Serial.print(";");
  Serial.print(average1);

  //sensor2
  // subtract the last reading:
  total2 = total2 - readings2[readIndex2];
  // read from the sensor:
  readings2[readIndex2] = valS2;
  // add the reading to the total:
  total2 = total2 + readings2[readIndex2];
  // advance to the next position in the array:
  readIndex2 = readIndex2 + 1;

  // if we're at the end of the array...
  if (readIndex2 >= numReadings) {
    // ...wrap around to the beginning:
    readIndex2 = 0;
  }
    // calculate the average:
  average2 = total2 / numReadings;
  Serial.print(";");
  Serial.println(average2);


  /*threshold = analogRead(A1);
  Serial.print(";");
  Serial.println(threshold);

  if (average > threshold)
  {
    digitalWrite(_OutPinBuzer, HIGH);
  }
  else
  {
    digitalWrite(_OutPinBuzer, LOW);
  }*/
  //wait
  delay(50);
}
