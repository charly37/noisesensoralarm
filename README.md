# Introduction
Noise sensor with variable alarm

## Hardware
Used IoT grove kit and more particularly:
Sound sensor : https://www.seeedstudio.com/Grove-Sound-Sensor-p-752.html?cPath=25_128
connected on A0
Buzzer : https://www.seeedstudio.com/Grove-Buzzer-p-768.html?cPath=38
connected on pin 4
Base shield : https://www.seeedstudio.com/Base-Shield-V2-p-1378.html?cPath=98_16
Roatry angle sensor : https://www.seeedstudio.com/Grove-Rotary-Angle-Sensor-p-770.html?cPath=85_52
connected on A1

on top of an arduino leonardo : https://www.arduino.cc/en/Main/ArduinoBoardLeonardo

## Software
Every XXXms the sound level is read from the sensor and smooth over the last 10 read (Reuse Arduino smoothing example : https://www.arduino.cc/en/Tutorial/Smoothing) to have a moving avergage. The position of the rotary sensor is also read at this same time and if the level of sounds is bigger than the read from the rotary the buzzzer will buzzz
